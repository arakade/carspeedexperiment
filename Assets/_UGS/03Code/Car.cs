﻿using Cinemachine;
using JetBrains.Annotations;
using UGS.carspeed.utils;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Scripting;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace UGS.carspeed {
	public class Car : MonoBehaviour {

		[SerializeField]
		private Vector3 acceleration = new Vector3(0f, 0f, 1f);

		[SerializeField]
		private ForceMode forceMode = ForceMode.VelocityChange;

		[SerializeField]
		private CinemachineVirtualCamera vCam = null;

		[SerializeField]
		private Rigidbody rb = null;

		[SerializeField]
		private ParticleSystem particles;

#if UNITY_EDITOR
		public void OnValidate() {
			if (!this.isPrefabModeActive()) {
				Assert.IsNotNull(vCam, $"{name} needs a {nameof(vCam)}");
			}
			if (null == rb) {
				Undo.RecordObject(this, "Set Rigidbody");
				rb = GetComponent<Rigidbody>();
				Assert.IsNotNull(rb);
			}
			if (null == particles) {
				Undo.RecordObject(this, "Set ParticleSystem");
				particles = GetComponent<ParticleSystem>();
				Assert.IsNotNull(particles, $"{name} needs a {nameof(ParticleSystem)}");
			}
		}
#endif

		public void Start() {
			startPos = rb.position;
			rb.AddForce(acceleration, forceMode);
		}

		public void setSpeed(float speed) {
			rb.velocity = new Vector3(0f, 0f, -speed); // speed negated since z inverted in world
		}

		[UsedImplicitly, Preserve]
		public void goToStart() {
			var deltaPos = startPos - rb.position;
			rb.MovePosition(startPos);
			vCam.OnTargetObjectWarped(transform, deltaPos);
		}

		[UsedImplicitly, Preserve]
		public void playParticles(bool play) {
			if (play) {
				particles.Play();
			} else {
				particles.Stop();
			}
		}

		private Vector3 startPos;
	}
}
