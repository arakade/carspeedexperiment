using UnityEngine;

namespace UGS.carspeed {
	public sealed class TriggerReturnToStart : MonoBehaviour {

		public void OnTriggerEnter(Collider other) {
			var car = other.GetComponentInParent<Car>();
			if (null == car)
				return;

			car.goToStart();
		}

	}
}
