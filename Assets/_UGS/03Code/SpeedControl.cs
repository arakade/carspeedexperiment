using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

namespace UGS.carspeed {
	public sealed class SpeedControl : MonoBehaviour {

		[SerializeField]
		private KeyCode keySpeedToggle = KeyCode.Space;

		[SerializeField]
		private ControlFenceSmear controlFenceSmear = null;

		[SerializeField]
		private Animator[] animators = null;

		[SerializeField]
		private string animTriggerEnable = "enable", animTriggerDisable = "disable";

		[SerializeField]
		private float smearAppearDelay = 0.5f;

		public void OnValidate() {
			Assert.IsNotNull(controlFenceSmear);
			Assert.IsNotNull(animators);
		}

		public void OnEnable() {
			Assert.IsNotNull(car);
		}

		public void Update() {
			if (Input.GetKeyDown(keySpeedToggle)) {
				StartCoroutine(enableSpeed(speedEnabled = !speedEnabled));
			}
		}

		public IEnumerator enableSpeed(bool enable) {
			controlFenceSmear.enableGeometry(true);
			foreach (var a in animators) {
				a.SetTrigger(enable ? animTriggerEnable : animTriggerDisable);
			}
			if (enable) {
				yield return new WaitForSeconds(smearAppearDelay);

				controlFenceSmear.enableSmear(true);
				car.playParticles(true);
			} else {
				controlFenceSmear.enableSmear(false);
				car.playParticles(false);
			}
		}

		private bool speedEnabled;

		private Car car {
			get {
				if (null == _car || !_car.isActiveAndEnabled) {
					_car = FindObjectOfType<Car>();
				}
				Assert.IsNotNull(_car);
				return _car;
			}
		}
		private Car _car;

	}
}
