using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using UnityEngine.Scripting;
using UnityEngine.UI;

namespace UGS.carspeed {
	public sealed class SwitchModels : MonoBehaviour {

		[SerializeField]
		private Button switchButton = null;

		[SerializeField]
		private Car car1 = null, car2 = null;

		public void OnValidate() {
			Assert.IsNotNull(switchButton);
			Assert.IsNotNull(car1);
			Assert.IsNotNull(car2);
		}

		public void OnEnable() {
			switchButton.onClick.AddListener(switchModel); // workaround Domain reload bug
		}

		public void OnDisable() {
			switchButton.onClick.RemoveListener(switchModel);
		}

		[UsedImplicitly, Preserve]
		public void switchModel() {
			EventSystem.current.SetSelectedGameObject(null); // remove focus so space won't re-press the button
			Car toDisable, toEnable;
			if (car1.isActiveAndEnabled) {
				toDisable = car1;
				toEnable = car2;
			} else {
				toDisable = car2;
				toEnable = car1;
			}
			toDisable.gameObject.SetActive(false);
			toEnable.gameObject.SetActive(true);
		}

	}
}
