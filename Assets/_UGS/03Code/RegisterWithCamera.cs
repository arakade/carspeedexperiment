using Cinemachine;
using UGS.carspeed.utils;
using UnityEngine;
using UnityEngine.Assertions;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace UGS.carspeed {
	public sealed class RegisterWithCamera : MonoBehaviour {

		[SerializeField]
		private CinemachineVirtualCamera vCam = null;

#if UNITY_EDITOR
		public void OnValidate() {
			if (this.isPrefabModeActive())
				return;

			Assert.IsNotNull(vCam);
			if (null == vCam || !vCam.enabled) {
				Undo.RecordObject(this, "Set CinemachineVirtualCamera");
				vCam = GetComponent<CinemachineVirtualCamera>();
				Assert.IsNotNull(vCam);
			}
		}
#endif

		public void OnEnable() {
			if (!Application.IsPlaying(this))
				return;

			var me = transform;
			vCam.Follow = me;
			vCam.LookAt = me;
		}

	}
}
