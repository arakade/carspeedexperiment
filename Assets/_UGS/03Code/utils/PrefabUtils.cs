using UnityEngine;

namespace UGS.carspeed.utils {
	public static class PrefabUtils {
		public static bool isPrefabModeActive(this GameObject gameObject) {
			return string.IsNullOrEmpty(gameObject.scene.path) || string.IsNullOrEmpty(gameObject.scene.name);
		}

		public static bool isPrefabModeActive(this Component component) {
			return isPrefabModeActive(component.gameObject);
		}

	}
}
