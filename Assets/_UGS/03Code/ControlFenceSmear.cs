using System.Linq;
using UnityEngine;

namespace UGS.carspeed {
	public sealed class ControlFenceSmear : MonoBehaviour {

		[SerializeField]
		private KeyCode keyGeometryToggle = KeyCode.G, keySmearToggle = KeyCode.S;

		[SerializeField]
		private string geometryName = "Geometry", smearName = "Smear";

		[SerializeField]
		private bool geometryStartOn = true, smearStartOn = false;

		public void Awake() {
			geometryObjects = FindObjectsOfType<GameObject>().Where(go => go.name == geometryName).ToArray();
			smearObjects = FindObjectsOfType<GameObject>().Where(go => go.name == smearName).ToArray();

			if (!geometryStartOn) {
				foreach (var o in geometryObjects) {
					o.SetActive(false);
				}
			}
			if (!smearStartOn) {
				foreach (var o in smearObjects) {
					o.SetActive(false);
				}
			}
		}

		public void Update() {
			if (Input.GetKeyDown(keyGeometryToggle)) {
				enableGeometry(geometryShowing = !geometryShowing);
			}

			if (Input.GetKeyDown(keySmearToggle)) {
				enableSmear(smearShowing = !smearShowing);
			}
		}

		public void enableGeometry(bool enable) {
			enableList(enable, geometryObjects);
		}

		public void enableSmear(bool enable) {
			enableList(enable, smearObjects);
		}

		private static void enableList(bool enable, GameObject[] list) {
			foreach (var go in list) {
				go.SetActive(enable);
			}
		}

		private GameObject[] geometryObjects;

		private GameObject[] smearObjects;

		private bool geometryShowing = true, smearShowing = false;

	}
}
