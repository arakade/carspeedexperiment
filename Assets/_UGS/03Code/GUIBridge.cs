using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace UGS.carspeed {
	public sealed class GUIBridge : MonoBehaviour {
#region serialized

		[SerializeField]
		private Slider speedSlider = null;

#endregion serialized
#region Unity callbacks

		public void OnValidate() {
			Assert.IsNotNull(speedSlider);
		}

		public void OnEnable() {
			if (null == car) {
				car = FindObjectOfType<Car>();
			}
			Assert.IsNotNull(car);

			speedSlider.onValueChanged.AddListener(setCarSpeed); // avoid Domain reload bug
		}

		public void OnDisable() {
			speedSlider.onValueChanged.RemoveListener(setCarSpeed);
		}

#endregion Unity callbacks
#region public

#endregion public
#region internal

#endregion internal
#region private

		private void setCarSpeed(float speed) {
			if (null == car || !car.isActiveAndEnabled) {
				car = FindObjectOfType<Car>();
			}
			Assert.IsNotNull(car);
			car.setSpeed(speed);
		}

		private Car car;

#endregion private
	}
}
