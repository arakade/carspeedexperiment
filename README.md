This is an experiment at implementing tricks to make a constant speed 'feel' faster.
When you press space, the speed doesn't change but a load of tricks are applied that might make it feel like you're travelling faster:

 * Camera FOV increased
 * Camera position moved closer
 * Camera wobbles dangerously
 * Blur textures applied between fence bars
 * Particles fly past the camera
 * PostFX are applied to the camera:
    * Motion Blur
    * Chromatic Aberration
    * Film grain
    
Try the built demo at https://arakade.itch.io/carspeedexp

Let me know your thoughts in its comments or over on Twitter [@Arakade](https://twitter.com/Arakade)
